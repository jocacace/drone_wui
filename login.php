<? 

	if ( $_POST["username"] == "" || $_POST["pass"] == ""  ) {
		include('index_no_field.html');
	}
	else {

		$servername = "127.0.0.1";
		$username = "root";
		$password = "dbpass";
		$dbname = "ASLA";
		$login_username = $_POST["username"]; 
		$login_password = $_POST["pass"]; 

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		
		// Check connection
		if ($conn->connect_error) {
		  die("Connection failed: " . $conn->connect_error);
		}

		$sql = "SELECT * from users;";

		$result = $conn->query($sql);

		$found = FALSE;


		if ($result->num_rows > 0) {
		  while( $found == FALSE && $row = $result->fetch_assoc()) {
				if( $login_username == $row["user"] && $login_password == $row["pass"] ) {
					$found = TRUE;
				}
			}
		}

		if( $found == TRUE ) {
				include('gcs/index.html');
		}

		else {
			include('index_wrong_auth.html');
		}
	}

?>
